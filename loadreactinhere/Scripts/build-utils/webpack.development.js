const path = require("path");
const webpack = require("webpack");
const WebpackNotifierPlugin = require("webpack-notifier");
const BrowserSyncPlugin = require("browser-sync-webpack-plugin");

module.exports = (env) => {
  console.log("starting dev");
  return {
    devtool: "inline-source-map",
    output: {
      filename: "[name].dev.bundle.js",
    },
    devServer: {
      port: env.port ? env.port : 3001,
      open: true,
    },
    plugins: [
      // new webpack.HotModuleReplacementPlugin(),
      new WebpackNotifierPlugin(),
      new BrowserSyncPlugin({
        port: env.port ? env.port : 3001,
      }),
      // {
      //   host: "localhost",
      //   port: env.port ? env.port : 3001,
      //   server: { baseDir: ["public"] },
      // },
      // {
      //   reload: false,
      // }
    ],
  };
};
