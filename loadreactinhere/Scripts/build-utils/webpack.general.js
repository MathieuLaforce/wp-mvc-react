const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = (env) => {
  console.log("general");
  console.log(process.cwd());
  return {
    mode: env.mode,
    entry: [
      "core-js/es/promise",
      "core-js/es/array/iterator",
      //   path.resolve(process.cwd(), "src/index.js"),
      path.resolve(__dirname, "../src/index.js"),
    ],
    output: {
      clean: true,
      //   path: path.resolve(process.cwd(), "dist"),
      path: path.resolve(__dirname, "../dist"),
    },
    externals: {
      react: "React",
      "react-dom": "ReactDOM",
    },
    module: {
      rules: [
        {
          test: /\.tsx?$/,
          use: ["babel-loader", "ts-loader"],
          exclude: /node_modules/,
        },
        {
          test: /\.(js|jsx)$/,
          exclude: /(node_modules)/,
          loader: "babel-loader",
          options: { presets: ["@babel/env"] },
        },
        {
          test: /\.css$/,
          use: ["style-loader", "css-loader"],
        },
      ],
    },
    plugins: [
      //   new HtmlWebpackPlugin({
      //     title: "learning",
      //     template: "index.html",
      //   }),
    ],
    resolve: {
      extensions: [".tsx", ".ts", ".js", ".jsx"],
    },
  };
};
