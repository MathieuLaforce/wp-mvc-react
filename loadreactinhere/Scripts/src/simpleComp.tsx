import React, { useEffect, useState } from "react";

import "./simpleComp.css";

interface Person {
  id: number;
  name: string;
  lastname: string;
}

const SimpleComp: React.FC<any> = (prop) => {
  const [person, setPerson] = useState<Person>({
    id: 2,
    lastname: "last",
    name: "first",
  });

  return (
    <div className="my-container">
      {Object.keys(person).map((p) => (
        <div key={p} className="child">
          {p}
        </div>
      ))}
      {Object.values(person).map((p) => (
        <div key={p} className="child">
          {p}
        </div>
      ))}
    </div>
  );
};

export default SimpleComp;
