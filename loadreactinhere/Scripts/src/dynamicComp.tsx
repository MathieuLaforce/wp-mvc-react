import React, { useEffect, useState } from "react";
const OtherComponent = React.lazy(() => import("./simpleComp"));

const DynamicComp: React.FC<any> = (prop) => {
  return (
    <div className="abc">
      <OtherComponent />
    </div>
  );
};

export default DynamicComp;
