import React, { Component, Suspense } from "react";
import DynamicComp from "./dynamicComp";
import Example from "./example";

// import "./App.css";

const App: React.FC = () => {
  return (
    <div className="App">
      <h1> Hello, what?</h1>
      <Example />
      <Suspense fallback={"die marie"}>
        <DynamicComp />
      </Suspense>
    </div>
  );
};

export default App;
