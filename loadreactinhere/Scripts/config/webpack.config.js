﻿const { merge } = require("webpack-merge");
const generalConfig = (env) => require(`../build-utils/webpack.general`)(env);
const modeConfig = (env) => require(`../build-utils/webpack.${env.mode}`)(env);

module.exports = (env) => {
  return merge(generalConfig(env), modeConfig(env));
};
